/**
 * View Model for the Search Activity , provides the items and manage the paging
 * @constructor repository , Injects a MLRepository
 *
 */
package com.example.demoappml

import android.app.Application
import com.example.demoappml.Neworking.MLApi

class MLApplication : Application() {

    lateinit var appComponent: MLAppComponent

    private fun initAppComponent(app: MLApplication): MLAppComponent {
        return DaggerMLAppComponent.builder()
            .mLAppModule(MLAppModule(app))
            .mLApi(MLApi()).build()
    }

    companion object {
        @get:Synchronized
        lateinit var application: MLApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        application = this
        appComponent = initAppComponent(this)
    }
}