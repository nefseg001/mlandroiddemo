/**
 * MLAppModule is the module app class for dagger
 *
 */
package com.example.demoappml

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MLAppModule(private val mlApplication: Application) {

    @Provides
    @Singleton
    fun provideContext(): Application = mlApplication
}