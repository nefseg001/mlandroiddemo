/**
 * View Model for the Search Activity , provides the items and manage the paging
 *
 */
package com.example.demoappml.itemDetail.viewmodel

import androidx.lifecycle.*
import com.example.demoappml.Neworking.DataLoadingState
import com.example.demoappml.Neworking.MLRepository
import com.example.demoappml.model.*
import kotlinx.coroutines.*
import javax.inject.Inject

class MLItemViewModel @Inject constructor(private val repository: MLRepository) :
    ViewModel() {

    private var debouncePeriod: Long = 500
    private var searchJob: Job? = null
    private var itemDetailLiveData: LiveData<MLItem>
    private val itemdIdLiveData = MutableLiveData<String>()
    var loadingStateLiveData = MutableLiveData<DataLoadingState>()
    val itemMediatorData = MediatorLiveData<MLItem>()


    private var itemID = ""

    init {
        itemDetailLiveData = Transformations.switchMap(itemdIdLiveData) {
            fetchItem(it)
        }
        loadingStateLiveData.value = DataLoadingState.READY
        itemMediatorData.addSource(itemDetailLiveData) {
            itemMediatorData.value = it
        }
    }

    fun geFetchItem(itemID: String) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            delay(debouncePeriod)
            itemdIdLiveData.value = itemID
        }
    }

    private fun fetchItem(itemID: String): LiveData<MLItem> {
        val liveData = MutableLiveData<MLItem>()
        viewModelScope.launch(Dispatchers.IO) {
            try {
                withContext(Dispatchers.Main) {
                    loadingStateLiveData.value =
                            DataLoadingState.LOADING
                }

                val model = repository.getItemDetail(itemID)
                liveData.postValue(model)
                loadingStateLiveData.postValue(DataLoadingState.LOADED)
            } catch (e: Exception) {
                loadingStateLiveData.postValue(DataLoadingState.ERROR_RESPONSE)
            }
        }
        return liveData
    }

    fun isDataLoaded (): Boolean{
        if (loadingStateLiveData.value == DataLoadingState.LOADED)
            return true
        return false
    }

}