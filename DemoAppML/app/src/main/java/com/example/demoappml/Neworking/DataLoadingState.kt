/**
 *
 * Enum used for knows the loading state
 */
package com.example.demoappml.Neworking

enum class DataLoadingState {
    READY,
    LOADING,
    LOADING_SAME,
    ERROR,
    LOADED,
    ERROR_RESPONSE
}