/**
 *
 * Data class container for paging items
 * @constructor Creates the MLPaging With JSON Response
 */
package com.example.demoappml.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MLPaging(
    @Json(name = "total")
    val total: String? = null,
    @Json(name = "primary_results")
    val results: String? = null,
    @Json(name = "offset")
    val offest: String? = null,
    @Json(name = "limit")
    val limit: String? = null
)