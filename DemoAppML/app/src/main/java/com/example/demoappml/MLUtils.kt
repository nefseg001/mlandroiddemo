/**
 * General Static class that provides methods for use in the activities
 *
 */
package com.example.demoappml

import android.app.AlertDialog
import android.content.Context
import android.widget.Toast
import com.example.demoappml.model.MLConstants
import com.example.demoappml.model.MLConstants.Companion.ML_CENTS_SYMBOL
import com.example.demoappml.model.MLConstants.Companion.ML_MONEY_SYMBOL
import com.example.demoappml.model.MLConstants.Companion.ML_NEW_ITEM
import com.example.demoappml.model.MLConstants.Companion.ML_USED_ITEM
import com.example.demoappml.model.MLItem

class MLUtils {

    companion object {
        val lmFormatData = { conditon: String? ->
            if (conditon.equals(MLConstants.CONDITION_NEW, false)) ML_NEW_ITEM
            else ML_USED_ITEM }
        val lmFormatPrice = { item: MLItem? ->
            ML_MONEY_SYMBOL + item?.priceItem + ML_CENTS_SYMBOL + item?.currency }

        fun createAlertWithMessage(message: String, context: Context) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(R.string.app_name)
            builder.setMessage(message)
            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                Toast.makeText(context,
                    android.R.string.yes, Toast.LENGTH_SHORT).show()
            }

            builder.setNegativeButton(android.R.string.no) { dialog, which ->
                Toast.makeText(context,
                    android.R.string.no, Toast.LENGTH_SHORT).show()
            }

            builder.show()
        }
    }

}