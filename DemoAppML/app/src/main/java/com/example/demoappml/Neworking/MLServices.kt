/**
 * Interface that contains the available services
 *
 */
package com.example.demoappml.Neworking

import com.example.demoappml.model.MLConstants
import com.example.demoappml.model.MLItem
import com.example.demoappml.model.MLResponseModel
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MLServices {

    @GET(MLConstants.GENERAL_GET_URL)
    fun getItems(@Query("q") qString: String, @Query("offset") offset: String, @Query("attributes") attrobutes: String): Deferred<Response<MLResponseModel>>


    @GET(MLConstants.GENERAL_GET_ITEM_URL)
    fun getItemDetail(@Path("item") item: String, @Query("attributes") attributes: String): Deferred<Response<MLItem>>
}

