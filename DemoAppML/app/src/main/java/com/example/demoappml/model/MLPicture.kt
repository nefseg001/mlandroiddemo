/**
 *
 * Data class container for the item pictures
 * @constructor Creates the MLPicture With JSON Response
 */
package com.example.demoappml.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MLPicture(
    @Json(name = "id")
    val idImage: String? = null,
    @Json(name = "secure_url")
    val url: String? = null
)