/**
 * Principal activity, search screen uses de MLSearchViewModel to manage the data
 *
 */
package com.example.demoappml.Search.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.demoappml.MLApplication
import com.example.demoappml.itemDetail.view.MLItemActivity
import com.example.demoappml.MLUtils
import com.example.demoappml.Neworking.DataLoadingState
import com.example.demoappml.R
import com.example.demoappml.model.*
import com.example.demoappml.model.MLConstants.Companion.ML_EXTRA_ID
import com.example.demoappml.model.MLConstants.Companion.ML_EXTRA_THUMBNAIL
import com.example.demoappml.model.MLConstants.Companion.ML_EXTRA_TITLE
import com.example.demoappml.Neworking.MLApiConnectivityLiveData
import com.example.demoappml.Search.viewmodel.MLSearchViewModel
import kotlinx.android.synthetic.main.activity_search.*
import javax.inject.Inject


class MLSearchActivity : AppCompatActivity(),
    MLSearchClickListener {

    private lateinit var searchActivityAdapter: MLSearchAdapterView

    private lateinit var mainViewModel: MLSearchViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var connectivityLiveData: MLApiConnectivityLiveData



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        initialize()
        initialiseObservers()
        initViews()
    }

    fun initialize() {
        connectivityLiveData =
            MLApiConnectivityLiveData(application)
        MLApplication.application.appComponent.inject(this)
        mainViewModel = ViewModelProvider(this, viewModelFactory).get(MLSearchViewModel::class.java)


    }

    private fun initViews() {
        searchRecyclerView.setHasFixedSize(true)
        searchRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        searchActivityAdapter =
            MLSearchAdapterView(this)
        searchRecyclerView.adapter = searchActivityAdapter
        run {
            searchRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (!searchRecyclerView.canScrollVertically(1)&&!mainViewModel.isLoadingPrevious()) {
                        if (dx!=0||dy!=0){
                            loadPageList()
                        }

                    }
                }
            })

        }
        buttonSearch.setOnClickListener(){
            requestSearch()

        }
        editTextSearch.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                requestSearch()
            }
            false
        })
    }

    private fun requestSearch(){
        val query = getQueryText()
        mainViewModel.onSearchQuery(query,"0")
        editTextSearch.text.clear()
        hideKeyboard(this)

    }

    fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun loadPageList() {
        mainViewModel.showNextPage()
    }

    private  fun getQueryText () : String {
        return editTextSearch.text.toString().replace("\n","", false);
    }


    private fun initialiseObservers() {
        mainViewModel.itemsMediatorData.observe(this, Observer {
            if(!mainViewModel.isLoadingPrevious() && !mainViewModel.isLoaded()){
                searchActivityAdapter.clearData()
            }
                searchActivityAdapter.updateData(it)

        })

        mainViewModel.loadingStateLiveData.observe(this, Observer {
            onLoadingStateChanged(it)
        })

        connectivityLiveData.observe(this, Observer { isAvailable ->

        })

    }


    private fun onLoadingStateChanged(state: DataLoadingState) {
        when (state) {
            DataLoadingState.LOADING -> {
                searchRecyclerView.visibility = View.GONE
                loadMoreProgress.visibility = View.VISIBLE
                textMessageNoData.visibility = View.GONE
            }
            DataLoadingState.LOADING_SAME -> {
                searchRecyclerView.visibility = View.VISIBLE
                loadMoreProgress.visibility = View.GONE
                textMessageNoData.visibility = View.GONE
            }
            DataLoadingState.LOADED -> {
                searchRecyclerView.visibility = View.VISIBLE
                loadMoreProgress.visibility = View.GONE
                textMessageNoData.visibility = View.GONE
                connectivityLiveData.value?.let {
                    if (it) {
                        searchRecyclerView.visibility = View.VISIBLE
                        loadMoreProgress.visibility = View.GONE
                        textMessageNoData.visibility = View.GONE

                    } else {
                        searchRecyclerView.visibility = View.GONE
                        loadMoreProgress.visibility = View.VISIBLE
                        textMessageNoData.visibility = View.GONE
                    }
                }
            }
            DataLoadingState.ERROR -> {
                searchRecyclerView.visibility = View.GONE
                textMessageNoData.visibility = View.VISIBLE
                loadMoreProgress.visibility = View.GONE
            }
            DataLoadingState.ERROR_RESPONSE -> {
                searchRecyclerView.visibility = View.GONE
                textMessageNoData.visibility = View.VISIBLE
                loadMoreProgress.visibility = View.GONE
                MLUtils.createAlertWithMessage(getString(R.string.title_error_message), this)
            }
        }
    }

    override fun onItemClicked(item: MLItem) {
        val intent = Intent(this, MLItemActivity::class.java).apply {
            putExtra(ML_EXTRA_ID, item.itemID)
            putExtra(ML_EXTRA_TITLE, item.titleItem)
            putExtra(ML_EXTRA_THUMBNAIL, item.thumbnailItem)

        }
        startActivity(intent)
    }


}