/**
 * ViewModel Module Provides the view models for the MLSearchActivity and MLItemActivity
 *
 */
package com.example.demoappml.viewmodel


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.Factory
import com.example.demoappml.Search.viewmodel.MLSearchViewModel
import com.example.demoappml.itemDetail.viewmodel.MLItemViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

@Singleton
class ViewModelFactory @Inject constructor(
    private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>) :
    Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        viewModels[modelClass]?.get() as T
}

@Module
abstract class MLViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): Factory

    @Binds
    @IntoMap
    @ViewModelKey(MLSearchViewModel::class)
    abstract fun mainViewModel(viewModel: MLSearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MLItemViewModel::class)
    abstract fun itemViewModel(viewModel: MLItemViewModel): ViewModel

}
