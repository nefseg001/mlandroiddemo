/**
 * Activity Class for show the item detail, use the MLItemViewModel to manage data
 */
package com.example.demoappml.itemDetail.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.example.demoappml.MLApplication
import com.example.demoappml.MLUtils
import com.example.demoappml.R
import com.example.demoappml.itemDetail.viewmodel.MLItemViewModel
import com.example.demoappml.Neworking.DataLoadingState
import com.example.demoappml.model.MLConstants
import com.example.demoappml.model.MLConstants.Companion.ML_EXTRA_ID
import com.example.demoappml.model.MLConstants.Companion.ML_EXTRA_THUMBNAIL
import com.example.demoappml.model.MLConstants.Companion.ML_EXTRA_TITLE
import com.example.demoappml.model.MLItem
import com.example.demoappml.Neworking.MLApiConnectivityLiveData
import kotlinx.android.synthetic.main.activity_item.*
import javax.inject.Inject


class MLItemActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var itemViewModel: MLItemViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)
        initialize()
        initialiseObservers()
        loadPrecharge()
    }

    private fun initialize() {
        MLApiConnectivityLiveData(application)
        MLApplication.application.appComponent.injectItem(this)
        itemViewModel = ViewModelProvider(this, viewModelFactory).get(MLItemViewModel::class.java)
        if (!itemViewModel.isDataLoaded()){
            var itemID = intent.getStringExtra(ML_EXTRA_ID)
            itemViewModel.geFetchItem(itemID.toString())
        }


    }

    fun loadPrecharge() {
        val thumbnail = intent.getStringExtra(ML_EXTRA_THUMBNAIL).toString().replace(MLConstants.NORMAL_HTTP,
            MLConstants.SECURE_HTTP, false)
        val title = intent.getStringExtra(ML_EXTRA_TITLE)
        presentPrecharge(thumbnail, title)
    }


    fun presentPrecharge(thumbnailImage: String?, title: String?) {
        intent.getStringExtra(ML_EXTRA_TITLE).let {
            titleDetailItem.text = it
        }
        val slideModels: MutableList<SlideModel> = ArrayList()
        slideModels.add(
            SlideModel(
                intent.getStringExtra(ML_EXTRA_THUMBNAIL).toString().replace(MLConstants.NORMAL_HTTP,
                MLConstants.SECURE_HTTP, false), ScaleTypes.CENTER_INSIDE
            )
        )
        imgDetailItem.setImageList(slideModels, ScaleTypes.CENTER_INSIDE)
    }



    private fun initialiseObservers() {
        itemViewModel.itemMediatorData.observe(this, Observer {
            updateUI(it)
        })
        itemViewModel.loadingStateLiveData.observe(this, Observer {
            onLoadingStateChanged(it)
        })

    }

    private fun onLoadingStateChanged(state: DataLoadingState) {
        when (state) {
            DataLoadingState.LOADING -> {
                progessLoadItem.visibility = View.VISIBLE
                groupItem.visibility = View.GONE
            }
            DataLoadingState.LOADED -> {
                progessLoadItem.visibility = View.GONE
                groupItem.visibility = View.VISIBLE
            }
            DataLoadingState.ERROR -> {
                groupItem.visibility = View.VISIBLE
                progessLoadItem.visibility = View.GONE
            }
            DataLoadingState.ERROR_RESPONSE -> {
                groupItem.visibility = View.VISIBLE
                progessLoadItem.visibility = View.GONE
                MLUtils.createAlertWithMessage(
                    getString(R.string.title_error_message),
                    this
                )
            }
        }
    }

    fun updateUI(itemModel: MLItem) {
        priceDetailItem.text =
            MLUtils.lmFormatPrice(itemModel)
        warrantyDetailItem.text = itemModel.warranty
        conditionDetailItem.text =
            MLUtils.lmFormatData(itemModel.condition)
        val slideModels: MutableList<SlideModel> = ArrayList()
        itemModel.pictures?.forEach {
            slideModels.add(
                SlideModel(
                    it.url.toString(), ScaleTypes.CENTER_INSIDE
                )
            )
        }
        imgDetailItem.setImageList(slideModels, ScaleTypes.CENTER_INSIDE)
    }
}