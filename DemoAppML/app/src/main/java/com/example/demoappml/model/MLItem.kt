/**
 * A group of *members*.
 *
 * Data class container for an item
 * @constructor Creates the MLItem With JSON Response
 */
package com.example.demoappml.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class MLItem(
    @Json(name = "id")
    val itemID: String? = null,
    @Json(name = "price")
    val priceItem: String? = null,
    @Json(name = "title")
    val titleItem: String? = null,
    @Json(name = "currency_id")
    val currency: String? = null,
    @Json(name = "thumbnail")
    val thumbnailItem: String? = null,
    @Json(name = "condition")
    val condition: String? = null,
    @Json(name = "pictures")
    val pictures: List<MLPicture>?,
    @Json(name = "warranty")
    val warranty: String? = null

)