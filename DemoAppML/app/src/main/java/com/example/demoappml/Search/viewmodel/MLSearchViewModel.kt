/**
 * View Model for the Search Activity , provides the items and manage the paging
 * @constructor repository , Injects a MLRepository
 *
 */
package com.example.demoappml.Search.viewmodel

import androidx.lifecycle.*
import com.example.demoappml.Neworking.DataLoadingState
import com.example.demoappml.Neworking.MLRepository
import com.example.demoappml.model.*
import kotlinx.coroutines.*
import javax.inject.Inject

class MLSearchViewModel @Inject constructor(private val repository: MLRepository) :
    ViewModel() {

    val itemsMediatorData = MediatorLiveData<List<MLItem>>()
    private var debouncePeriod: Long = 500
    private var searchJob: Job? = null
    private var searchItemsLiveData: LiveData<MLResponseModel>

    val loadingStateLiveData = MutableLiveData<DataLoadingState>()
    private val _searchFieldTextLiveData = MutableLiveData<String>()
    private var actualOffset = ""


    init {
        searchItemsLiveData = Transformations.switchMap(_searchFieldTextLiveData) {
            fetchItemsWithQuery(it, actualOffset)
        }


        itemsMediatorData.addSource(searchItemsLiveData) {
            if(isLoadingPrevious()) {
                itemsMediatorData.value = it.items?.plus(itemsMediatorData.value!!)
            } else {
                itemsMediatorData.value = it.items
            }

        }
    }

    fun onSearchQuery(query: String, offset: String) {
        searchJob?.cancel()
        loadingStateLiveData.value = DataLoadingState.READY
        searchJob = viewModelScope.launch {
            delay(debouncePeriod)
            if (query.length > 2) {
                actualOffset = offset
                _searchFieldTextLiveData.value = query
            }
        }
    }

    private fun fetchItemsWithQuery(query: String, offset: String): LiveData<MLResponseModel> {
        val liveData = MutableLiveData<MLResponseModel>()

        viewModelScope.launch(Dispatchers.IO) {
            try {
                withContext(Dispatchers.Main) {
                    if (offset.toInt() > 0)
                        loadingStateLiveData.value =
                            DataLoadingState.LOADING_SAME
                    else
                        loadingStateLiveData.value =
                            DataLoadingState.LOADING
                }

                val model = repository.getItems(query, offset)
                liveData.postValue(model)
                loadingStateLiveData.postValue(DataLoadingState.LOADED)
            } catch (e: Exception) {
                loadingStateLiveData.postValue(DataLoadingState.ERROR_RESPONSE)
            }
        }
        return liveData
    }

    fun showNextPage() {
        var paging = searchItemsLiveData.value?.paging
        if (paging?.results?.toInt()!! > searchItemsLiveData.value?.items?.size!!) {
            searchJob?.cancel()
            loadingStateLiveData.value = DataLoadingState.READY
            searchJob = viewModelScope.launch {
                delay(debouncePeriod)
                actualOffset = (paging.offest?.toString()?.toInt()!! + 1).toString()
                _searchFieldTextLiveData.value = _searchFieldTextLiveData.value

            }
        }


    }

    fun isLoadingPrevious() : Boolean {
        if (loadingStateLiveData.value == DataLoadingState.LOADING_SAME)
            return true
        return false
    }

    fun isLoaded() : Boolean {
        if (loadingStateLiveData.value == DataLoadingState.LOADED)
            return true
        return false
    }

    override fun onCleared() {
        super.onCleared()
        searchJob?.cancel()
    }
}