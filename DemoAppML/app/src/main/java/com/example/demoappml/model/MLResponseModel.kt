/**
 *
 * Data class container for a search response
 * @constructor Creates the MLResponse With JSON Response
 */
package com.example.demoappml.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MLResponseModel(
    val page: String?,
    @Json(name = "results")
    val items: List<MLItem>?,
    @Json(name = "query")
    val query: String?,
    @Json(name = "paging")
    val paging: MLPaging?
)