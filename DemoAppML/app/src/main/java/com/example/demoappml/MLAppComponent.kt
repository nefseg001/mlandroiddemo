/**
 * App Component interface for Dagger
 *
 */
package com.example.demoappml

import com.example.demoappml.Neworking.MLApi
import com.example.demoappml.itemDetail.view.MLItemActivity
import com.example.demoappml.viewmodel.MLViewModelModule
import com.example.demoappml.Search.view.MLSearchActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [MLAppModule::class, MLApi::class, MLViewModelModule::class])
interface MLAppComponent {
    fun inject(mainActivity: MLSearchActivity)
    fun injectItem(itemActivity: MLItemActivity)

}