/**
 *
 * Constants Class
 */
package com.example.demoappml.model

interface MLConstants {
    companion object {
        const val GENERAL_GET_URL = "sites/MLM/search"
        const val GENERAL_GET_ITEM_URL = "items/{item}"

        const val BASE_URL = "https://api.mercadolibre.com/"
        const val REQUIRED_SEARCH_ATTRIBUTES = "paging,results,query"
        const val REQUIRED_ITEM_ATTRIBUTES = "id,title,price,currency_id,condition,thumbnail,warranty,pictures"


        const val CONDITION_NEW = "new"
        const val NORMAL_HTTP = "http"
        const val SECURE_HTTP = "https"

        const val ML_EXTRA_ID = "ml_Id"
        const val ML_EXTRA_TITLE = "ml_title"
        const val ML_EXTRA_THUMBNAIL = "ml_thumbnail"

        const val ML_NEW_ITEM = "Nuevo"
        const val ML_USED_ITEM = "Usado"

        const val ML_MONEY_SYMBOL = "$"
        const val ML_CENTS_SYMBOL = ".00"

    }
}