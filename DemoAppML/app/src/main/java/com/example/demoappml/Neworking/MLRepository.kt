/**
 *
 * Repository Class for get the data from services
 * @constructor Creates a MLRepository with a services interface
 */
package com.example.demoappml.Neworking

import com.example.demoappml.model.MLConstants.Companion.REQUIRED_ITEM_ATTRIBUTES
import com.example.demoappml.model.MLConstants.Companion.REQUIRED_SEARCH_ATTRIBUTES
import com.example.demoappml.model.MLItem
import com.example.demoappml.model.MLResponseModel
import javax.inject.Inject

class MLRepository @Inject constructor(private val mlService: MLServices) {

    suspend fun getItems(queryText: String, offset: String): MLResponseModel? {
        val deferredResponse = mlService.getItems(queryText, offset, REQUIRED_SEARCH_ATTRIBUTES).await()
        return if (deferredResponse.isSuccessful) {
            deferredResponse.body()
        } else {
            throw Exception()
        }
    }

    suspend fun getItemDetail(mlIdItem: String): MLItem? {
        val deferredResponse = mlService.getItemDetail( mlIdItem, REQUIRED_ITEM_ATTRIBUTES).await()
        return if (deferredResponse.isSuccessful) {
            deferredResponse.body()
        } else {
            throw Exception()
        }
    }
}