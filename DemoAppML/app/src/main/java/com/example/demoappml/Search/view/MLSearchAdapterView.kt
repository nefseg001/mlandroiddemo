/**
 * Adapter for the search item list, need the search click listener owner
 *
 */
package com.example.demoappml.Search.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide
import com.example.demoappml.MLUtils.Companion.lmFormatData
import com.example.demoappml.MLUtils.Companion.lmFormatPrice
import com.example.demoappml.R;
import com.example.demoappml.model.MLConstants.Companion.NORMAL_HTTP
import com.example.demoappml.model.MLConstants.Companion.SECURE_HTTP
import com.example.demoappml.model.MLItem
import kotlinx.android.synthetic.main.item_search_row.view.*


interface MLSearchClickListener {
    fun onItemClicked(item: MLItem)
}



class MLSearchAdapterView(private val searchClickListener: MLSearchClickListener)
    : RecyclerView.Adapter<MLSearchAdapterView.itemViewHolder>() {

    private var data = mutableListOf<MLItem?>()

    inner class itemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: MLItem?) {
            itemView.txtTitle.text = "${data[position]?.titleItem}"
            itemView.txtPrice.text = lmFormatPrice(data[adapterPosition])
            itemView.txtCondition.text = lmFormatData(lmFormatData(data[adapterPosition]?.condition))
            Glide
                .with(itemView)
                .load(data[position]?.thumbnailItem?.replace(NORMAL_HTTP, SECURE_HTTP, false))
                .centerCrop()
                .override(150,150)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(itemView.thumbnail)
            itemView.setOnClickListener {
                data[position]?.let {
                    searchClickListener.onItemClicked(it)
                }
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): itemViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.item_search_row,
            parent,
            false
        )

        return itemViewHolder(itemView)
    }


        override fun getItemCount(): Int {
            return data.size
        }

        fun updateData(newData: List<MLItem?>) {
            data.addAll(newData)
            notifyDataSetChanged()
        }

        fun refreshScreen () {
            notifyDataSetChanged()
        }

        fun clearData() {
            data.clear()
        }

    override fun onBindViewHolder(holder: itemViewHolder, position: Int) {
        holder.bind(data[position])
    }


}