package com.example.demoappml


import com.example.demoappml.model.MLItem
import com.example.demoappml.model.MLPaging
import com.example.demoappml.model.MLResponseModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MLResponseModel  {
    lateinit var responseModel: MLResponseModel


    @Before
    fun setUp(){
        val item = MLItem("itemid","3000", "title", "MXN", "thmbnail","new", null)
        val list = listOf<MLItem>(item)

        responseModel = MLResponseModel("page", list,"motorola", null)
    }
    @Test
    fun thestResponseConstructor() {
        assertEquals(responseModel.page, "page")
        assertEquals(responseModel.query, "motorola")
    }

}