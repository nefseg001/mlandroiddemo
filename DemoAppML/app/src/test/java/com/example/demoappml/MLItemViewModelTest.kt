package com.example.demoappml

import com.example.demoappml.Neworking.DataLoadingState
import com.example.demoappml.Neworking.MLRepository
import com.example.demoappml.itemDetail.viewmodel.MLItemViewModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mock

class MLItemViewModelTest  {
    lateinit var mlItemModelVi: MLItemViewModel

    @Mock
    lateinit var repositoryModel: MLRepository

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
       mlItemModelVi =
           MLItemViewModel(
               repositoryModel
           )
        mlItemModelVi.loadingStateLiveData.value = DataLoadingState.ERROR
    }
    @Test
    fun thestSearchquery() {
      mlItemModelVi.geFetchItem("")
    }

}