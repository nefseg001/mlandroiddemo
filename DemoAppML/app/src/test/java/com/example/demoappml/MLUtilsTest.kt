package com.example.demoappml

import com.example.demoappml.model.MLConstants
import com.example.demoappml.model.MLItem
import org.junit.Assert.assertEquals
import org.junit.Test

val lmFormatData = { conditon: String? ->
    if (conditon.equals(MLConstants.CONDITION_NEW, false)) MLConstants.ML_NEW_ITEM
    else MLConstants.ML_USED_ITEM
}
val lmFormatPrice = { item: MLItem? ->
    MLConstants.ML_MONEY_SYMBOL + item?.priceItem + MLConstants.ML_CENTS_SYMBOL + item?.currency }

class MLUtilsTest  {

    @Test
    fun testFormatData() {
        val new = lmFormatData("new")
        assertEquals(new, "new")
        val used = lmFormatData("new")
        assertEquals(used, "used")
    }

    @Test
    fun testFormatPrice() {
        val item = MLItem("itemid","3000", "title", "MXN", "thmbnail","new", null)
        val price = lmFormatPrice(item)
        assertEquals(price, "$3000.00 MXN")
    }

}