package com.example.demoappml

import com.example.demoappml.model.MLItem
import com.example.demoappml.model.MLPicture
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MLItemTest {
    lateinit var item: MLItem
    @Before
    fun setUp(){
        var picture = MLPicture()
        var list = listOf<MLPicture>(picture)
        item = MLItem("MLM857306702","123",
            "Moto E7 Plus Dual Sim 64 Gb Azul Mystic 4 Gb Ram", "MXN",
            "http://mlm-s1-p.mlstatic.com/734503-MLA44053829307_112020-I.jpg", "new", list)
    }
    @Test
    fun testItemCOostructor() {
        assertEquals(item.itemID, "MLM857306702")
        assertEquals(item.priceItem, "123")
        assertEquals(item.titleItem, "Moto E7 Plus Dual Sim 64 Gb Azul Mystic 4 Gb Ram")
        assertEquals(item.currency, "MXN")
        assertEquals(item.thumbnailItem, "http://mlm-s1-p.mlstatic.com/734503-MLA44053829307_112020-I.jpg")
        assertEquals(item.condition, "new")
    }

}