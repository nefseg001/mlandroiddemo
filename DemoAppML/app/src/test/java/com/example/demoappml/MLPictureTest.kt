package com.example.demoappml

import com.example.demoappml.model.MLPicture
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MLPictureTest {
    lateinit var picture: MLPicture
    @Before
    fun setUp(){
        picture = MLPicture("734503-MLA44053829307_112020","http://mlm-s1-p.mlstatic.com/734503-MLA44053829307_112020-O.jpg")
    }
    @Test
    fun testPictureConstructor() {
        assertEquals(picture.idImage, "734503-MLA44053829307_112020")
        assertEquals(picture.url, "http://mlm-s1-p.mlstatic.com/734503-MLA44053829307_112020-O.jpg")
    }

}